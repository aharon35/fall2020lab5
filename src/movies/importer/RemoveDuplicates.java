//Aharon Moryoussef
//1732787
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{

	public RemoveDuplicates(String srcDir , String destDir) {
	super(srcDir, destDir, false);
		
	}
	
	public ArrayList<String> process (ArrayList<String> input) {
		ArrayList<String> rm = new ArrayList<String>();
	    
	    for (String s : input) {
	    	if(!rm.contains(s)) {
	    		rm.add(s);
	    	}
	    }
	  
		
	return rm;
	
}
}


